﻿namespace Task1;

public static class Server
{
    private static int currentUserID;
    public static List<Ad> ads = new List<Ad>();
    public static List<User> users = new List<User>();

    public static void Login()
    {
        int input;

        Console.Clear();
        Console.WriteLine("=== ВХОД ===");
        Console.Write("1 - Регистрация\n2 - Авторизация\n3 - Выйти из приложения\n");
        input = Convert.ToInt32(Console.ReadLine());
        
        if (input <= 1)
        {
            currentUserID = Register();
            ViewCurrentUser();
        }
        
        else if (input == 2)
        {
            currentUserID = Authorize();
            ViewCurrentUser();
        }
    }
    
    private static int Register()
    {
        int id = GetUserID();
        string name;
        string phoneNumber = "";
        string email = "";
        string password = "0";
        string tempPassword = "1";
        string codeWord = "admin";
        string inputWord;
        bool flag = false;

        Console.Clear();
        Console.WriteLine("=== РЕГИСТРАЦИЯ ===");

        Console.Write("Введите своё имя: ");
        name = Console.ReadLine();

        while (!flag)
        {
            flag = true;
            Console.Write("Введите свой номер телефона: ");
            phoneNumber = Console.ReadLine();
            Console.Write("Введите адрес своей электронной почты: ");
            email = Console.ReadLine();

            if (phoneNumber == "" || email == "")
            {
                Console.WriteLine("\nНомер телефона и адрес электронной почты не могут быть пустыми.\n");
                flag = false;
                continue;
            }
            
            for (int i = 0; i < users.Count; i++)
            {
                if (phoneNumber == users[i].phoneNumber || email == users[i].email)
                {
                    Console.WriteLine("\nЭтот номер телефона / адрес электронной почты уже используется.\n");
                    flag = false;
                    break;
                }
            }
        }
        
        while (flag)
        {
            Console.Write("Введите свой пароль: ");
            password = Console.ReadLine();
            Console.Write("Повторите свой пароль: ");
            tempPassword = Console.ReadLine();

            if (password == tempPassword && password == "")
            {
                Console.WriteLine("\nПароль не может быть пустым.\n");
            }
            
            else if (password != tempPassword)
            {
                Console.WriteLine("\nЗначения не совпадают. Попробуйте ещё раз.\n");
            }

            else
            {
                flag = false;
            }
        }
        
        Console.Write("\nНапишите кодовое слово, чтобы стать модератором (либо нажмите Enter, чтобы пропустить): ");
        inputWord = Console.ReadLine();
        
        if (inputWord == codeWord)
        {
            Console.WriteLine("Права модератора получены.");
            users.Add(new Moderator(id, name, phoneNumber, email, password));
        }
        
        else if (inputWord != "")
        {
            Console.WriteLine("Неверное кодовое слово. Права модератора не получены.");
            users.Add(new User(id, name, phoneNumber, email, password));
        }
        
        else
        {
            users.Add(new User(id, name, phoneNumber, email, password));
        }
        Console.WriteLine("\nРегистрация прошла успешно! Нажмите любую клавишу, чтобы продолжить.");
        Console.ReadLine();
        return id;
    }

    private static int Authorize()
    {
        string login;
        string password;
        int id = 0;
        bool flag = false;
        
        Console.Clear();
        Console.WriteLine("=== АВТОРИЗАЦИЯ ===");

        while (!flag)
        {
            Console.Write("Введите логин (номер телефона или адрес электронной почты): ");
            login = Console.ReadLine();
            Console.Write("Введите пароль: ");
            password = Console.ReadLine();

            for (int i = 0; i < users.Count; i++)
            {
                if ((login == users[i].phoneNumber || login == users[i].email) &&
                    password == users[i].password)
                {
                    id = users[i].id;
                    Console.Write("Вход прошёл успешно! Нажмите любую клавишу, чтобы продолжить.");
                    flag = true;
                    break;
                } 
            }

            if (flag)
            {
                break;
            }
            
            Console.WriteLine("\nНеверный логин или пароль. Повторите попытку.\n");
        }

        Console.ReadLine();
        return id;
    }
    
    private static void Logout()
    {
        currentUserID = 0;
        Login();
    }

    public static Ad SearchAdByID(int id)
    {
        foreach (Ad ad in ads)
        {
            if (ad.id == id)
            {
                return ad;
            }
        }
        return ads[0];
    }
    
    public static User SearchUserByID(int id)
    {
        foreach (User user in users)
        {
            if (user.id == id)
            {
                return user;
            }
        }
        return users[0];
    }

    public static int GetAdID()
    {
        int id = 1;
        bool flag = false;
        
        while (!flag)
        {
            flag = true;
            
            foreach (Ad ad in ads)
            {
                if (ad.id == id)
                {
                    id++;
                    flag = false;
                    break;
                }
            }
        }
        
        return id;
    }

    public static int GetUserID()
    {
        int id = 1;
        bool flag = false;

        while (!flag)
        {
            flag = true;
            
            foreach (User user in users)
            {
                if (user.id == id)
                {
                    id++;
                    flag = false;
                    break;
                }
            }
        }

        return id;
    }

    public static void ViewCurrentUser()
    {
        int input;
        User currentUser = SearchUserByID(currentUserID);
        
        Console.Clear();
        Console.WriteLine($"=== {currentUser.name.ToUpper()} ===");
        Console.Write("1 - Создать объявление\n2 - Просмотреть свои объявления\n3 - Просмотреть чужие объявления\n4 - Просмотреть других пользователей\n5 - Выйти из аккаунта\n");
        input = Convert.ToInt32(Console.ReadLine());

        if (input <= 1)
        {
            currentUser.Post();
        }
        
        else if (input == 2)
        {
            currentUser.View();
        }
        
        else if (input == 3)
        {
            ViewAds();
        }
        
        else if (input == 4)
        {
            ViewUsers();
        }

        else
        {
            Logout();
        }
    }
    
    private static void ViewAds()
    {
        int input_1;
        int input_2;
        int j = 1;
        User currentUser = SearchUserByID(currentUserID);
        Dictionary<int, int> indexes = new Dictionary<int, int>();

        Console.Clear();
        Console.WriteLine("=== ЧУЖИЕ ОБЪЯВЛЕНИЯ ===");
        
        for (int i = 0; i < ads.Count; i++)
        {
            if (ads[i].owner != currentUser)
            {
                Console.WriteLine($"{j} - {ads[i].header} - {ads[i].owner.name}");
                indexes.Add(j, i);
                j++;
            }
        }

        Console.Write("Выберите объявление (0, чтобы вернуться назад): ");
        input_1 = Convert.ToInt32(Console.ReadLine());
        
        if (input_1 <= 0)
        {
            ViewCurrentUser();
        }
        
        else
        {
            if (input_1 > ads.Count)
            {
                input_1 = ads.Count;
            }
            ads[indexes[input_1]].View();
            Console.Clear();
            Console.WriteLine("=== ОБЪЯВЛЕНИЕ ===");
            ads[indexes[input_1]].View();
            Console.WriteLine("\n=== ПОЛЬЗОВАТЕЛЬ ===");
            ads[indexes[input_1]].owner.Info();

            if (currentUser.GetType().Name == "Moderator")
            {
                Moderator m = (Moderator) currentUser;
                Console.Write("\n1 - Перейти к пользователю\n2 - Оценить пользователя\n3 - Удалить объявление\n4 - Удалить пользователя\n5 - Выйти\n");
                input_2 = Convert.ToInt32(Console.ReadLine());

                if (input_2 <= 1)
                {
                    currentUser.View(ads[input_1 - 1].owner.id);
                }
            
                else if (input_2 == 2)
                {
                    currentUser.RateUser(ads[input_1 - 1].owner.id);
                }
                
                else if (input_2 == 3)
                {
                    m.DeleteUserAd(ads[input_1 - 1].id);
                }
                
                else if (input_2 == 4)
                {
                    m.DeleteUser(ads[input_1 - 1].owner.id);
                }

                else
                {
                    ViewCurrentUser();
                }
            }

            else
            {
                Console.Write("\n1 - Перейти к пользователю\n2 - Оценить пользователя\n3 - Выйти\n");
                input_2 = Convert.ToInt32(Console.ReadLine());

                if (input_2 <= 1)
                {
                    currentUser.View(ads[input_1 - 1].owner.id);
                }
            
                else if (input_2 == 2)
                {
                    currentUser.RateUser(ads[input_1 - 1].owner.id);
                }

                else
                {
                    ViewCurrentUser();
                }
            }
        }
    }
    
    private static void ViewUsers()
    {
        int input_1;
        int j = 1;
        User currentUser = SearchUserByID(currentUserID);
        Dictionary<int, int> indexes = new Dictionary<int, int>();
        
        Console.Clear();
        Console.WriteLine("=== ДРУГИЕ ПОЛЬЗОВАТЕЛИ ===");
        
        for (int i = 0; i < users.Count; i++)
        {
            if (users[i] != currentUser)
            {
                Console.WriteLine($"{j} - {users[i].name}");
                indexes.Add(j, i);
                j++;
            }
        }

        Console.Write("Выберите пользователя (0, чтобы вернуться назад): ");
        input_1 = Convert.ToInt32(Console.ReadLine());
        if (input_1 <= 0)
        {
            ViewCurrentUser();
        }
        
        else
        {
            if (input_1 > users.Count)
            {
                input_1 = users.Count;
            }
            currentUser.View(users[indexes[input_1]].id);
        }
    }
}