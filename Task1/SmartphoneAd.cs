﻿namespace Task1;

public class SmartphoneAd : Ad
{
    public string manufacturer;
    public string model;
    public int ram;
    public int memory;
    public double diagonal;
    public string color;

    public override void Init()
    {
        base.Init();
        Console.Write("Производитель: ");
        manufacturer = Console.ReadLine();
        Console.Write("Модель: ");
        model = Console.ReadLine();
        Console.Write("Объём оперативной памяти (Гб): ");
        ram = Convert.ToInt32(Console.ReadLine());
        Console.Write("Общий объём памяти на накопителях (Гб): ");
        memory = Convert.ToInt32(Console.ReadLine());
        Console.Write("Диагональ экрана (дюймы, использовать запятую): ");
        diagonal = Convert.ToDouble(Console.ReadLine());
        Console.Write("Цвет: ");
        color = Console.ReadLine();
    }
    
    public override void View()
    {
        base.View();
        Console.WriteLine($"Производитель\t{manufacturer}");
        Console.WriteLine($"Модель\t\t{model}");
        Console.WriteLine($"ОЗУ\t\t{ram} Гб");
        Console.WriteLine($"Память\t\t{memory} Гб (всего)");
        Console.WriteLine($"Диагональ\t{diagonal} дюймов");
        Console.WriteLine($"Цвет\t\t{color}");
    }
}