﻿namespace Task1;

public abstract class Factory
{
    public abstract Ad FactoryMethod();
}