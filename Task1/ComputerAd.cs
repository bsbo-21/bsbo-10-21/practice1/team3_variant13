﻿namespace Task1;

public class ComputerAd : Ad
{
    public string cpu;
    public string gpu;
    public int ram;
    public int memory;

    public override void Init()
    {
        base.Init();
        Console.Write("Модель процессора: ");
        cpu = Console.ReadLine();
        Console.Write("Модель видеокарты: ");
        gpu = Console.ReadLine();
        Console.Write("Объём оперативной памяти (Гб): ");
        ram = Convert.ToInt32(Console.ReadLine());
        Console.Write("Общий объём памяти на накопителях (Гб): ");
        memory = Convert.ToInt32(Console.ReadLine());
    }

    public override void View()
    {
        base.View();
        Console.WriteLine($"Процессор\t{cpu}");
        Console.WriteLine($"Видеокарта\t{gpu}");
        Console.WriteLine($"ОЗУ\t\t{ram} Гб");
        Console.WriteLine($"Память\t\t{memory} Гб (всего)");
    }
}