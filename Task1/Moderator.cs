﻿namespace Task1;

public class Moderator : User
{
    public Moderator(int id, string name, string phoneNumber, string email, string password) : base(id, name, phoneNumber, email, password){}
    
    public void DeleteUserAd(int id)
    {
        Ad ad = Server.SearchAdByID(id);
        ad.owner.ads.Remove(ad);
        Server.ads.Remove(ad);
        Console.WriteLine("\nОбъявление успешно удалено! Нажмите любую клавишу, чтобы продолжить.");
        Console.ReadLine();
        Server.ViewCurrentUser();
    }

    public void DeleteUser(int id)
    {
        User user = Server.SearchUserByID(id);
        int i = 0;
        while (i < Server.ads.Count)
        {
            if (Server.ads[i].owner == user)
            {
                Server.ads.RemoveAt(i);
            }
            else
            {
                i++;
            }
        }
        
        Server.users.Remove(user);
        Console.WriteLine("\nПользователь успешно удалён! Нажмите любую клавишу, чтобы продолжить.");
        Console.ReadLine();
        Server.ViewCurrentUser();
    }
    
    public override void View(int id)
    {
        int input_1;
        int input_2;
        User user = Server.SearchUserByID(id);
        
        Console.Clear();
        
        Console.WriteLine($"=== {user.name.ToUpper()} ===");
        
        for (int i = 0; i < user.ads.Count; i++)
        {
            Console.WriteLine($"{i + 1} - {user.ads[i].header}");
        }

        Console.Write("Выберите объявление (0, чтобы вернуться назад): ");
        input_1 = Convert.ToInt32(Console.ReadLine());
        
        if (input_1 <= 0)
        {
            Server.ViewCurrentUser();
        }
        
        else
        {
            Console.Clear();
            
            if (input_1 > user.ads.Count)
            {
                input_1 = user.ads.Count;
            }
            
            Console.WriteLine("=== ОБЪЯВЛЕНИЕ ===");
            user.ads[input_1 - 1].View();
            Console.WriteLine("\n=== ПОЛЬЗОВАТЕЛЬ ===");
            user.Info();
            Console.Write("\n1 - Оценить пользователя\n2 - Удалить объявление\n3 - Удалить пользователя\n4 - Выйти\n");
            input_2 = Convert.ToInt32(Console.ReadLine());
                
            if (input_2 <= 1)
            {
                RateUser(user.id);
            }
            
            else if (input_2 == 2)
            {
                DeleteUserAd(user.ads[input_1 - 1].id);
            }
                
            else if (input_2 == 3)
            {
                DeleteUser(user.id);
            }
            
            else
            {
                Server.ViewCurrentUser();
            }
        }
    }
}