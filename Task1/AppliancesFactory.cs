﻿namespace Task1;

public class AppliancesFactory : Factory
{
    public override Ad FactoryMethod()
    {
        return new AppliancesAd();
    }
}