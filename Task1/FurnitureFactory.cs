﻿namespace Task1;

public class FurnitureFactory : Factory
{
    public override Ad FactoryMethod()
    {
        return new FurnitureAd();
    }
}