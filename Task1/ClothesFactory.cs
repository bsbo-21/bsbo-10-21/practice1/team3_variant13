﻿namespace Task1;

public class ClothesFactory : Factory
{
    public override Ad FactoryMethod()
    {
        return new ClothesAd();
    }
}