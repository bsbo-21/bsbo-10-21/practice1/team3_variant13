﻿namespace Task1;

public class FurnitureAd : Ad
{
    public string type;
    public string material;
    public int length;
    public int width;
    public int height;

    public override void Init()
    {
        base.Init();
        Console.Write("Тип: ");
        type = Console.ReadLine();
        Console.Write("Материал: ");
        material = Console.ReadLine();
        Console.Write("Длина (мм): ");
        length = Convert.ToInt32(Console.ReadLine());
        Console.Write("Ширина (мм): ");
        width = Convert.ToInt32(Console.ReadLine());
        Console.Write("Высота (мм): ");
        height = Convert.ToInt32(Console.ReadLine());
    }
    
    public override void View()
    {
        base.View();
        Console.WriteLine($"Тип\t\t{type}");
        Console.WriteLine($"Материал\t{material}");
        Console.WriteLine($"Длина\t\t{length} мм");
        Console.WriteLine($"Ширина\t\t{width} мм");
        Console.WriteLine($"Высота\t\t{height} мм");
    }
}