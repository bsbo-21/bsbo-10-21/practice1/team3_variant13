﻿namespace Task1;

public class User
{
    public int id;
    public string name;
    public string phoneNumber;
    public string email;
    public string password;
    public float rating;
    public Dictionary<User, int> marks;
    public List<Ad> ads;
    private Ad lastAd;
    private Factory factory;

    public User(int id, string name, string phoneNumber, string email, string password)
    {
        this.id = id;
        this.name = name;
        this.phoneNumber = phoneNumber;
        this.email = email;
        this.password = password;
        rating = 0.0f;
        ads = new List<Ad>();
        marks = new Dictionary<User, int>();
    }

    public void Post()
    {
        int input;
        
        Console.Clear();
        Console.WriteLine("=== КАТЕГОРИЯ ===");
        Console.Write("1 - компьютеры\n2 - смартфоны\n3 - бытовая техника\n4 - одежда\n5 - мебель\n");
        input = Convert.ToInt32(Console.ReadLine());

        if (input <= 1)
        {
            factory = new ComputerFactory();
        }
        else if (input == 2)
        {
            factory = new SmartphoneFactory();
        }
        else if (input == 3)
        {
            factory = new AppliancesFactory();
        }
        else if (input == 4)
        {
            factory = new ClothesFactory();
        }
        else
        {
            factory = new FurnitureFactory();
        }

        Console.Clear();
        lastAd = factory.FactoryMethod();
        lastAd.owner = this;
        lastAd.Init();
        ads.Add(lastAd);
        Server.ads.Add(lastAd);
        Console.WriteLine("\nОбъявление успешно создано! Нажмите любую клавишу, чтобы продолжить.");
        Console.ReadLine();
        Server.ViewCurrentUser();
    }

    public void Edit(int id)
    {
        Ad ad = Server.SearchAdByID(id);
        Console.WriteLine("\nИспользуйте стрелки вверх / вниз, чтобы использовать предыдущие значения параметров.");
        ad.Init();
        Console.WriteLine("\nОбъявление успешно изменено! Нажмите любую клавишу, чтобы продолжить.");
        Console.ReadLine();
        Server.ViewCurrentUser();
    }

    public void Delete(int id)
    {
        for (int i = 0; i < ads.Count; i++)
        {
            if (id == ads[i].id)
            {
                ads.RemoveAt(i);
                break;
            }
        }

        for (int i = 0; i < Server.ads.Count; i++)
        {
            if (id == Server.ads[i].id)
            {
                Server.ads.RemoveAt(i);
                break;
            }
        }
        
        Console.WriteLine("\nОбъявление успешно удалено! Нажмите любую клавишу, чтобы продолжить.");
        Console.ReadLine();
        Server.ViewCurrentUser();
    }

    public void RateUser(int id)
    {
        int input;
        int sum = 0;
        User user = Server.SearchUserByID(id);

        if (user.marks.ContainsKey(this))
        {
            Console.Write("\nВы уже оценивали этого пользователя. Нажмите любую клавишу, чтобы продолжить.");
        }
        
        else
        {
            Console.Clear();
            Console.WriteLine($"=== {user.name.ToUpper()} ===");
            Console.WriteLine("\nОцените пользователя от 1 до 5: ");
            input = Convert.ToInt32(Console.ReadLine());
        
            if (input <= 1)
            {
                user.marks.Add(this, 1);
            }
        
            else if (input == 2)
            {
                user.marks.Add(this, 2);
            }
        
            else if (input == 3)
            {
                user.marks.Add(this, 3);
            }
        
            else if (input == 4)
            {
                user.marks.Add(this, 4);
            }
        
            else
            {
                user.marks.Add(this, 5);
            }

            foreach (int value in user.marks.Values)
            {
                sum += value;
            }
        
            user.rating = (float) sum / user.marks.Count;
            Console.Write("\nСпасибо за ваш отзыв! Нажмите любую клавишу, чтобы продолжить.");
        }
        
        Console.ReadLine();
        Server.ViewCurrentUser();
    }

    public void View()
    {
        int input_1;
        int input_2;
        
        Console.Clear();
        Console.WriteLine("=== СВОИ ОБЪЯВЛЕНИЯ ===");
        
        for (int i = 0; i < ads.Count; i++)
        {
            Console.WriteLine($"{i + 1} - {ads[i].header}");
        }

        Console.Write("Выберите объявление (0, чтобы вернуться назад): ");
        input_1 = Convert.ToInt32(Console.ReadLine());
        
        if (input_1 <= 0)
        {
            Server.ViewCurrentUser();
        }
        
        else
        {
            if (input_1 > ads.Count)
            {
                input_1 = ads.Count;
            }
            
            Console.Clear();
            ads[input_1 - 1].View();
            Console.Write("\n1 - Редактировать\n2 - Удалить\n3 - Выйти\n");
            input_2 = Convert.ToInt32(Console.ReadLine());

            if (input_2 <= 1)
            {
                Edit(ads[input_1 - 1].id);
            }
                
            else if (input_2 == 2)
            {
                Delete(ads[input_1 - 1].id);
            }

            else
            {
                Server.ViewCurrentUser();
            }
        }
    }
    
    public virtual void View(int id)
    {
        int input_1;
        int input_2;
        User user = Server.SearchUserByID(id);
        
        Console.Clear();
        
        Console.WriteLine($"=== {user.name.ToUpper()} ===");
        
        for (int i = 0; i < user.ads.Count; i++)
        {
            Console.WriteLine($"{i + 1} - {user.ads[i].header}");
        }

        Console.Write("Выберите объявление (0, чтобы вернуться назад): ");
        input_1 = Convert.ToInt32(Console.ReadLine());
        
        if (input_1 <= 0)
        {
            Server.ViewCurrentUser();
        }
        
        else
        {
            if (input_1 > user.ads.Count)
            {
                input_1 = user.ads.Count;
            }
            
            Console.Clear();
            Console.WriteLine("=== ОБЪЯВЛЕНИЕ ===");
            user.ads[input_1 - 1].View();
            Console.WriteLine("\n=== ПОЛЬЗОВАТЕЛЬ ===");
            user.Info();
            Console.Write("\n1 - Оценить пользователя\n2 - Выйти\n");
            input_2 = Convert.ToInt32(Console.ReadLine());
                
            if (input_2 <= 1)
            {
                RateUser(id);
            }
                
            else
            {
                Server.ViewCurrentUser();
            }
        }
    }

    public void Info()
    {
        Console.WriteLine($"Пользователь\t{name}");
        Console.WriteLine($"Телефон\t\t{phoneNumber}");
        Console.WriteLine($"Почта\t\t{email}");
        Console.WriteLine($"Рейтинг\t\t{rating} / 5");
    }
}