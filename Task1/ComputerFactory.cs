﻿namespace Task1;

public class ComputerFactory : Factory
{
    public override Ad FactoryMethod()
    {
        return new ComputerAd();
    }
}