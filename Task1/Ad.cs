﻿namespace Task1;

public abstract class Ad
{
    public int id;
    public User owner;
    public string header;
    public string desc;
    public int price;
    public Photo photo;

    public virtual void Init()
    {
        if (id == 0)
        {
            id = Server.GetAdID();
        }
        Console.Write("Заголовок: ");
        header = Console.ReadLine();
        Console.Write("Описание: ");
        desc = Console.ReadLine();
        Console.Write("Цена (руб.): ");
        price = Convert.ToInt32(Console.ReadLine());
        Console.Write("Фото (URL-ссылка): ");
        photo.link = Console.ReadLine();
    }

    public virtual void View()
    {
        Console.WriteLine($"Заголовок\t{header}");
        Console.WriteLine($"Описание\t{desc}");
        Console.WriteLine($"Цена\t\t{price} руб.");
        Console.WriteLine($"Фото\t\t{photo.link}\n");
    }
}