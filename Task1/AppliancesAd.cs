﻿namespace Task1;

public class AppliancesAd : Ad
{
    public string type;
    public string manufacturer;
    public string model;

    public override void Init()
    {
        base.Init();
        Console.Write("Тип: ");
        type = Console.ReadLine();
        Console.Write("Производитель: ");
        manufacturer = Console.ReadLine();
        Console.Write("Модель: ");
        model = Console.ReadLine();
    }
    
    public override void View()
    {
        base.View();
        Console.WriteLine($"Тип\t\t{type}");
        Console.WriteLine($"Производитель\t{manufacturer}");
        Console.WriteLine($"Модель\t\t{model}");
    }
}