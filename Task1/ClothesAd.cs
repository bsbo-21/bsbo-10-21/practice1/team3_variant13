﻿namespace Task1;

public class ClothesAd : Ad
{
    public string type;
    public int size;
    public string material;
    public string color;

    public override void Init()
    {
        base.Init();
        Console.Write("Тип: ");
        type = Console.ReadLine();
        Console.Write("Размер: ");
        size = Convert.ToInt32(Console.ReadLine());
        Console.Write("Материал: ");
        material = Console.ReadLine();
        Console.Write("Цвет: ");
        color = Console.ReadLine();
    }
    
    public override void View()
    {
        base.View();
        Console.WriteLine($"Тип\t\t{type}");
        Console.WriteLine($"Размер\t\t{size}");
        Console.WriteLine($"Материал\t{material}");
        Console.WriteLine($"Цвет\t\t{color}");
    }
}