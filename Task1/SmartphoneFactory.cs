﻿namespace Task1;

public class SmartphoneFactory : Factory
{
    public override Ad FactoryMethod()
    {
        return new SmartphoneAd();
    }
}