```plantuml
@startuml ClassDiagram
class Server{
    int currentUserID
    Ad[] ads
    User[] users
    void Login()
    int Register()
    int Authorize()
    void Logout()
    Ad SearchAdByID()
    User SearchUserByID()
    int GetAdID()
    int GetUserID()
}

class Client{
    void Main()
}

class User{
    int id
    string name
    string phone
    string email
    string password
    float rating
    int[] marks
    Ad[] ads
    Ad lastAd
    Factory factory
    User()
    void Post()
    void Edit()
    void Delete()
    void RateUser()
}

class Moderator{
    void DeleteUserAd()
    void DeleteUser()
}

abstract class Ad{
    User owner
    string name
    string desc
    string condition
    int price
    Photo[] photos
    void Init()
}

struct Photo{
    string link
}

class ComputerAd{
    string cpu
    string gpu
    int ram
    int memory
    void Init()
}

class SmartphoneAd{
    string manufacturer
    string model
    int ram
    int memory
    float diagonal
    string color
    void Init()
}

class AppliancesAd{
    string type
    string manufacturer
    string model
    void Init()
}

class ClothesAd{
    string type
    int size
    string color
    void Init()
}

class FurnitureAd{
    string type
    string material
    int length
    int width
    int height
    void Init()
}

abstract class Factory{
    Ad FactoryMethod()
}

class ComputerFactory{
    Ad FactoryMethod()
}

class SmartphoneFactory{
    Ad FactoryMethod()
}

class AppliancesFactory{
    Ad FactoryMethod()
}

class ClothesFactory{
    Ad FactoryMethod()
}

class FurnitureFactory{
    Ad FactoryMethod()
}

Moderator --|> User
Ad *-- Photo
ComputerAd --|> Ad
SmartphoneAd --|> Ad
AppliancesAd --|> Ad
ClothesAd --|> Ad
FurnitureAd --|> Ad
ComputerFactory ..> ComputerAd
SmartphoneFactory ..> SmartphoneAd
AppliancesFactory ..> AppliancesAd
ClothesFactory ..> ClothesAd
FurnitureFactory ..> FurnitureAd
User *-- Factory
ComputerFactory --|> Factory
SmartphoneFactory --|> Factory
AppliancesFactory --|> Factory
ClothesFactory --|> Factory
FurnitureFactory --|> Factory
Server o-- Ad
Server o-- User
Ad --> User
User o-- Ad
Client --> Server
User --> Server
User ..> Moderator
Server ..> Moderator
@enduml
```