```plantuml
@startuml Register
actor Пользователь
loop Пока телефон и почта не уникальны
Пользователь -> Server : Запрос на проверку
activate Server
alt уникальны
    Server --> Пользователь: Успешная регистрация
else иначе
    Server -> Пользователь: Ошибка регистрации
end
end
alt кодовое слово указано верно
    Server -> Moderator: создание модератора
else иначе
    Server -> User: создание пользователя
end
Server -> Server: авторизация текущего пользователя
deactivate Server
@enduml
```