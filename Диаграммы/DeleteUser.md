```plantuml
@startuml Register
actor Модератор
Модератор -> Server : DeleteUser()
activate Server
Server -> Server : поиск пользователя по id
loop Для всех объявлений
alt владелец объявления - выбранный пользователь
    Server -> Ad : удаление объявления пользователя
end
end
Server -> User : удаление пользователя
deactivate Server
@enduml
```
