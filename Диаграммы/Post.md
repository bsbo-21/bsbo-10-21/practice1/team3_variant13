```plantuml
@startuml Post
actor Пользователь
Пользователь -> User : Post()
activate User
User -> Factory : FabricMethod()
activate Factory
Factory -> ConcreteFactory : переопределение FabricMethod()
deactivate Factory
activate ConcreteFactory
ConcreteFactory -> User : возврат объявления нужного подкласса
deactivate ConcreteFactory
User -> ConcreteAd : Init()
activate ConcreteAd
ConcreteAd -> Ad : base.Init()
activate Ad
Ad -> ConcreteAd : инициализация общих полей
deactivate Ad
ConcreteAd -> User : инициализация специфических полей
deactivate ConcreteAd
User -> User : добавление текущего объявления в список пользователя
User -> Server: добавление объявления на сервер
deactivate User
@enduml
```