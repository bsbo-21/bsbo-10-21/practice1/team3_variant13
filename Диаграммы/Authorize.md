```plantuml
@startuml Authorize
actor Пользователь
loop Пока телефон / почта и пароль не совпадают
Пользователь -> Server : Запрос на проверку
activate Server
Server -> Server : Поиск пользователя в списке
alt совпадают
    Server --> Пользователь: Успешный вход
else иначе
    Server -> Пользователь: Ошибка входа
end
end
Server -> Server: авторизация текущего пользователя
deactivate Server
@enduml
```