```plantuml
@startuml UseCaseDiagram
actor "Пользователь" as user
actor "Модератор" as moderator
user <|-right- moderator
left to right direction
rectangle "Электронная доска объявлений" {
  usecase "Зарегистрироваться / войти в систему" as authorize
  usecase "Разместить объявление" as post
  usecase "Редактировать объявление" as edit
  usecase "Удалить объявление" as delete
  usecase "Оценить другого пользователя" as rateUser
  usecase "Выйти из системы" as logout
  usecase "Удалить обявление другого пользователя" as deleteUserAd
  usecase "Удалить пользователя" as deleteUser
}
user --> authorize
user --> post
user --> delete
user --> edit
user --> rateUser
user --> logout
moderator --> deleteUserAd
moderator --> deleteUser
@enduml
```